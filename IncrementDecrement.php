

<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>

<h2 style="text-align: center";>Incremnet Decrement</h2>

<form style="width: 600px;padding-left: 325px;margin-left: 10px";>
<table>
    <tr>
        <th>value of a:5</th>
        <th>value of b:7</th>
        <th>value of x:9</th>
        <th>value of y:3</th>
    </tr>
    <tr>
        <td>post Increment <?php $a=5; echo $a++?></td>
        <td>pre Increment <?php $b=7; echo ++$b?></td>
        <td>post Decrement <?php $x=9; echo $x--?></td>
        <td>pre Decrement <?php $y=3; echo --$y?></td>
    </tr>

</table>
</form>
</body>
</html>